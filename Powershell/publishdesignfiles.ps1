﻿Add-PSSnapin microsoft.sharepoint.powershell
$allsites = get-spsite -WebApplication "https://sp13e" -Limit all 

foreach ($site in $allsites){


$masterurl = $site | Get-SPWeb
$web = $masterurl[0]
$masterurl = $masterurl[0].masterurl

Write-Host $masterurl -ForegroundColor Green
Write-Host $web -ForegroundColor red

$folder = $web.GetFolder("_catalogs/masterpage")

 $folder.Files | ?{ $_.Item -ne $null -and $_.Item.Properties["HtmlDesignLockedFile"] -eq $null -and $_.MinorVersion -ne 0 } | %{

        Write-Host ("Publishing {0}" -f $_.Url);

        $_.Publish("design package deployment");

    }

   



$site.Dispose()

}