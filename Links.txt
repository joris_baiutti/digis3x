SharePoint Color Tool
https://www.microsoft.com/en-us/download/details.aspx?id=38182

Breadcrumb
http://jenasaswati.blogspot.ch/2013/12/breadcrumb-navigation-in-sharepoint-2013.html

Channel 9 Branding Session
https://channel9.msdn.com/events/Ignite/2015/BRK3164

Display Templates Blog
http://news.digicomp.ch/de/2015/05/20/sharepoint-2013-suchergebnisse-anpassen-mit-display-templates/

Bootstrap
http://getbootstrap.com/

Less
http://lesscss.org/

Sass
http://sass-lang.com/

Foundation
http://foundation.zurb.com/

JQuery
https://jquery.com/

PowerBI
https://powerbi.microsoft.com/de-de/

Responsive SharePoint
https://responsivesharepoint.codeplex.com/

Ebooks
http://it-ebooks.info/
